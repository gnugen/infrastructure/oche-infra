# Config and Documentation for the `oche` VM running on otemma
> This VM runs a Nextcloud with Onlyoffice for OChE – Orchestre de Chambre des Etudiant·e·s de Lausanne (EPFL student association).

## Installation basics
The VM is running Debian stable (currently: bullseye).
Install docker-compose and its dependencies using apt:
```
aptitude install docker-compose
```
Install git to clone this repository:
```
aptitue install git
```
Clone this repository into `/etc/oche-infra`:
```
cd /etc/
git clone https://gitlab.gnugen.ch/gnugen/oche-infra.git
```
Create the `db.env` and `oo.env` files from their corresponding `.sample` file.  
Replace the values (especially the passwords/secrets) with real values.


## Install or Update
To update, do the following steps:
1. System updates
   Update the operating system as usual using aptitude.
   ```
   aptitude update && aptitude upgrade
   ```
2. Application updates (Docker)
   Switch into the directory containing the docker-compose config
   ```
   cd /etc/oche-infra
   ```
   Download the latest container versions:
   ```
   docker-compose pull
   ```
   Switch to the new version:
   ```
   docker-compose up -d --build
   ```
   Clean up old versions (images):
   ```
   docker images
   ```
   take a look at the images and copy the IMAGE ID of the ones which are not used anymore (generally the ones with TAG <none>)  
   and delete them using
   ```
   docker rmi <IMAGE ID>
   ```
   prune unused volumes
   ```
   docker volume prune
   ```

### Major Nextcloud versions
(Do this some weeks after a new major version has been released, usually when the first point release is available)
Change the version number in the `docker-compose.yml` file at the *two* ocurrences (in `nextcloud-app` and `nextcloud-cron`):
```
    image: nextcloud:<major_version>-fpm-alpine
```
:warning: Always make sure to update from one major version to the next at a time. Otherwise Nextcloud won't be able to do database migrations correctly.  
:warning: Also note that downgrading is not supported by Nextcloud. (As well due to database migrations.)

After those changes have been committed you can pull the latest state on the server and follow the regular update procedure.


## Configuration
In Nextclouds config/config.php:
- set trusted domains (cloud.oche.ch, oche-cloud.epfl.ch, nextcloud-web)
- set 'overwrite.cli.url' => 'https://cloud.oche.ch/',
- set 'overriteprotocol' => true,
- set 'activity_use_cached_mountpoints' => true, (for groupfolders activity)

### install apps
- calendar
- contacts
- onlyoffice
- forms
- polls

### configure onlyoffice
- Docs address: https://oche-onlyoffice.epfl.ch/
- secret from oo.env
- internal docs address: http://onlyoffice-documentserver/
- internal server address: http://nextcloud-web/


## Useful facts when working with Docker
- volume mounts are stored in `/var/lib/docker/volumes/`
- get a shell inside a running container `docker-compose exec <containername> sh`

## Inspired by
- https://github.com/nextcloud/docker
- https://github.com/nextcloud/docker/blob/master/.examples/docker-compose/with-nginx-proxy/postgres/fpm/docker-compose.yml
- https://github.com/ONLYOFFICE/Docker-DocumentServer
- https://github.com/ONLYOFFICE/docker-onlyoffice-nextcloud